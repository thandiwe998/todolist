<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Todocontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index',[Todocontroller::class,'index'])->name('todo.index');
Route::get('/create',[Todocontroller::class,'create']);
Route::post('/upload',[Todocontroller::class, 'upload']);
Route::get('/{id}/edit',[Todocontroller::class, 'edit']);
Route::patch('/{id}/update',[Todocontroller::class, 'update']);
Route::get('/{id}/completed',[Todocontroller::class, 'completed']);
Route::get('/{id}/delete',[Todocontroller::class, 'delete']);